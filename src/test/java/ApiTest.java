import static io.restassured.RestAssured.*;
import static org.hamcrest.core.IsEqual.equalTo;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Random;

public class ApiTest {
    static StringBuilder name = new StringBuilder(10);
    static String orderId;
    static String authoToken;
    @Test (priority = 1)
    public void getMethod(){
        Response response = get("https://reqres.in/api/users?page=2");
        Assert.assertEquals(200,response.getStatusCode());
    }
    @Test (priority = 2)
    public void findStatusCode(){
        RequestSpecification request = RestAssured.given();
        request.baseUri("https://simple-books-api.glitch.me");
                request.basePath("/status");
        Response response = request.get();
        Assert.assertEquals(200,response.getStatusCode());
    }
    @Test (priority = 3)
    public void findAuthenticationToken(){
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"+"lmnopqrstuvwxyz!@#$%&";
        Random temp = new Random();
        for (int i = 0; i < 10; i++) {
            name.append(chars.charAt(temp.nextInt(chars.length())));
        }
        baseURI ="https://simple-books-api.glitch.me";
        RequestSpecification request = given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("clientName",""+name);
        requestParams.put("clientEmail",name+"@gmail.com");
        request.header("Content-Type", "application/json");
        request.body(requestParams.toString());
        Response response = request.post("/api-clients");
        Assert.assertEquals(response.getStatusCode(),201);
        authoToken = response.jsonPath().get("accessToken");
    }
    @Test (priority = 4)
    public void postMethod(){
        baseURI ="https://simple-books-api.glitch.me";
        JSONObject requestParams = new JSONObject();
        requestParams.put("bookId", 1);
        requestParams.put("customerName", ""+name);
        orderId = given()
                .header("Content-Type", "application/json")
                .header("Authorization","Bearer "+ authoToken)
                .body(requestParams.toString())
                .when()
                .post("/orders")
                .then()
                .statusCode(201)
                .body("created",equalTo(true))
                .extract()
                .path("orderId");
    }
    @Test (priority = 5)
    public void patchMethod() {
        baseURI ="https://simple-books-api.glitch.me";
        JSONObject requestParams = new JSONObject();
        requestParams.put("customerName", "john");
        given()
                .header("Authorization","Bearer "+ authoToken)
                .body(requestParams.toString())
                .when()
                .patch("/orders/"+orderId)
                .then()
                .statusCode(204);
    }
    @Test (priority = 6)
    public void deleteMethod(){
        baseURI ="https://simple-books-api.glitch.me";
        JSONObject requestParams = new JSONObject();
        requestParams.put("customerName", "john");
        given()
                .header("Authorization","Bearer "+ authoToken)
                .body(requestParams.toString())
                .when()
                .delete("/orders/"+orderId)
                .then()
                .statusCode(204);
    }
}
